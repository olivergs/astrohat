# Astrohat

A RaspberryPi4 compatible hat for all your astronomy equipment. Features include:

* 6 12V controllable outputs @3A each with current monitoring (2 PWM controllable for dew heaters)
* Temperature, Humidity and Pressure sensor port (external module)
* 1 adjustable 6-12 V output
* Port for serial communication and power to external device (GPS)y
* Automotive grade electonics and design

![Astrohat](astrohat.png)

# Contribute

Schematic and PCB are designed on KiCAD v5.1

# License

Licensed under the [CERN OHLv1.2](LICENSE) 2019-2020 Pierros Papadeas.
